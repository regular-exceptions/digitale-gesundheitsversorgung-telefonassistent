# Digitale Gesundheitsversorgung Telefonassistent
A WirVsVirus Project

## Setup 
### Dialogflow
Set the fulfillment endpoint to your local machine (HTTPS tunnel required!)

### Your machine development
- clone project
- install or have npm installed
- `npm i` in project main dir
- `npm run tsc` - compiles ts to js
- `npm run start` - runs the agent backend

### Setup (docker)
- coming soon (tm)
