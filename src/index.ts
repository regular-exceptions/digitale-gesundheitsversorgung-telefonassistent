import {dialogflow} from 'actions-on-google'
import * as bodyParser from 'body-parser'
import Log from "./Helpers/Log"
import * as express from 'express'

const agent = main()

const expressApp = express().use(bodyParser.json())

expressApp.post('/fulfillments', agent)

Log.info("Now listening on port 3000")
expressApp.listen(3000)


agent.middleware(async conv => {
  // middlewarestuff
})


agent.fallback((conv) => {
  Log.info("Fallback case")
  conv.ask('Ich habe Sie nicht verstanden.')
})

function main() {
  return dialogflow({debug: true})
}




