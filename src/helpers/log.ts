import {ConsoleColors} from "./ConsoleColors"

export default class Log {
  static info(message, ...args) {
    console.log(Log.annotate(message, "INFO"), ...args)
  }

  static warning(message, ...args) {
    console.log(Log.color(ConsoleColors.FgYellow), Log.annotate(message, "WARNING"), ...args)
  }

  static error(message, ...args) {
    console.log(Log.color(ConsoleColors.FgRed), Log.annotate(message, "ERROR"), ...args)
  }

  static success(message, ...args) {
    console.log(Log.color(ConsoleColors.FgGreen), Log.annotate(message, "SUCCESS"), ...args)
  }

  private static color(color: ConsoleColors) {
    return `${color}%s${ConsoleColors.Reset}`
  }

  private static annotate(message: string, type: string) {
    return `${new Date().toISOString()} - [${type}] - ${message}`
  }
}
